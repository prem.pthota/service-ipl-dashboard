/**
 * 
 */
package com.ipl.dashboard.service;

import com.ipl.dashboard.domain.Team;

/**
 * @author Polaiah Thota
 *
 */
public interface TeamService {

	Team getTeam(String teamName);
}
