/**
 * 
 */
package com.ipl.dashboard.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ipl.dashboard.domain.Team;
import com.ipl.dashboard.repository.MatchRepository;
import com.ipl.dashboard.repository.TeamRepository;
import com.ipl.dashboard.service.TeamService;

/**
 * @author Polaiah Thota
 *
 */
@Service
public class TeamServiceImpl implements TeamService {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private MatchRepository matchRepository;

	@Override
	public Team getTeam(String teamName) {
		Team team = new Team();
		try {

			team = teamRepository.findByTeamName(teamName);
			if (team != null) {
				Pageable pageable = PageRequest.of(0, 4);
				team.setMatchs(matchRepository.getByTeam1OrTeam2OrderByDateDesc(teamName, teamName, pageable));
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
			throw e;
		}
		return team;
	}

}
