/**
 * 
 */
package com.ipl.dashboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ipl.dashboard.domain.Team;

/**
 * @author Polaiah Thota
 *
 */
public interface TeamRepository extends JpaRepository<Team, Integer> {

	Team findByTeamName(String teamName);

}
