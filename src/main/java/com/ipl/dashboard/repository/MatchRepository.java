/**
 * 
 */
package com.ipl.dashboard.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ipl.dashboard.domain.Match;

/**
 * @author Polaiah Thota
 *
 */
public interface MatchRepository extends JpaRepository<Match, Integer> {

	List<Match> getByTeam1OrTeam2OrderByDateDesc(String team1, String team2, Pageable pageable);

}
