/**
 * 
 */
package com.ipl.dashboard.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipl.dashboard.domain.Team;
import com.ipl.dashboard.service.TeamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Polaiah Thota
 *
 */
@RestController
@RequestMapping("/ipl")
@Api(tags = "IPL Dashboard API")
public class TeamController {
	@Autowired
	private TeamService teamService;

	@GetMapping("/team/{teamName}")
	@ApiOperation(value = "IPL Dashboard")
	@ApiResponses(value = { //
			@ApiResponse(code = 200, response = Team.class, message = "OK"),
			@ApiResponse(code = 400, message = "Bad Request"), //
			@ApiResponse(code = 403, message = "Access Denied") })
	public ResponseEntity<Team> getTeam(@PathVariable(name = "teamName") String teamName) {
		return ResponseEntity.ok(teamService.getTeam(teamName));
	}

}
