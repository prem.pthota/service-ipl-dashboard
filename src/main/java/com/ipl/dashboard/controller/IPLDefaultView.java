/**
 * 
 */
package com.ipl.dashboard.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Polaiah Thota
 *
 */
@ApiIgnore
@Controller
public class IPLDefaultView {
	
	
	@RequestMapping(value = "/")
	public String page() {
		
		return "redirect:/swagger-ui.html";
	}

}
