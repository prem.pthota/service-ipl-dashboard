/**
 * 
 */
package com.ipl.dashboard.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.ipl.dashboard.domain.Match;

/**
 * @author Polaiah Thota
 *
 */
@SpringBootTest
@DisplayName("When Running MatchRepository Class")
class MatchRepositoryTest {

	@MockBean
	private MatchRepository matchRepository;

	Match match;

	Sort sort;

	Pageable pageable;

	Iterable<Integer> ids;

	Iterable<Match> agEntities;

	Example<Match> agExample;

	@BeforeEach
	void init() {
		match = new Match();
	}

	/**
	 * Test method for
	 * {@link com.ipl.dashboard.repository.MatchRepository#getByTeam1OrTeam2OrderByDateDesc(java.lang.String, java.lang.String, org.springframework.data.domain.Pageable)}.
	 */
	@Test
	@DisplayName("Testing GetByTeam1OrTeam2OrderByDateDesc Method")
	void testGetByTeam1OrTeam2OrderByDateDesc() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll()}.
	 */
	@Test
	@DisplayName("Testing FindAll Method")
	void testFindAll() {
		when(matchRepository.findAll()).thenReturn(Stream.of(match).collect(Collectors.toList()));
		assertEquals(1, matchRepository.findAll().size());
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllSort() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAllById(java.lang.Iterable)}.
	 */
	@Test
	void testFindAllByIdIterableOfID() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAll(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllIterableOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#flush()}.
	 */
	@Test
	void testFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAndFlush(S)}.
	 */
	@Test
	void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAllAndFlush(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllAndFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllInBatchIterableOfT() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllByIdInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllByIdInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllInBatch()}.
	 */
	@Test
	void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#getOne(java.lang.Object)}.
	 */
	@Test
	void testGetOne() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#getById(java.lang.Object)}.
	 */
	@Test
	void testGetById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllSort1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)}.
	 */
	@Test
	void testFindAllPageable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#save(S)}.
	 */
	@Test
	void testSave() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#saveAll(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllIterableOfS1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findById(java.lang.Object)}.
	 */
	@Test
	void testFindById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#existsById(java.lang.Object)}.
	 */
	@Test
	void testExistsById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findAll()}.
	 */
	@Test
	void testFindAll1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findAllById(java.lang.Iterable)}.
	 */
	@Test
	void testFindAllByIdIterableOfID1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#count()}.
	 */
	@Test
	void testCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteById(java.lang.Object)}.
	 */
	@Test
	void testDeleteById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#delete(java.lang.Object)}.
	 */
	@Test
	void testDelete() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAllById(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAll(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllIterableOfQextendsT() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAll()}.
	 */
	@Test
	void testDeleteAll() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findOne(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindOne() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindAllExampleOfS1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllExampleOfSSort1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Pageable)}.
	 */
	@Test
	void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#count(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#exists(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testExists() {
		fail("Not yet implemented");
	}

}
