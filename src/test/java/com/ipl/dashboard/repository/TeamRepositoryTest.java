/**
 * 
 */
package com.ipl.dashboard.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.ipl.dashboard.domain.Team;

/**
 * @author Polaiah Thota
 *
 */
@SpringBootTest
@DisplayName("When Running TeamRepository Class")
class TeamRepositoryTest {

	@MockBean
	private TeamRepository teamRepository;

	Team team;

	Sort sort;

	Pageable pageable;

	Iterable<Integer> ids;

	Iterable<Team> agEntities;

	Example<Team> agExample;

	@BeforeEach
	void init() {
		team = new Team();
	}

	/**
	 * Test method for
	 * {@link com.ipl.dashboard.repository.TeamRepository#findByTeamName(java.lang.String)}.
	 */
	@Test
	@DisplayName("Testing FindByTeamName Method")
	void testFindByTeamName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll()}.
	 */
	@Test
	@DisplayName("Testing FindAll Method")
	void testFindAll() {
		when(teamRepository.findAll()).thenReturn(Stream.of(team).collect(Collectors.toList()));
		assertEquals(1, teamRepository.findAll().size());
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllSort() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAllById(java.lang.Iterable)}.
	 */
	@Test
	void testFindAllByIdIterableOfID() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAll(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllIterableOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#flush()}.
	 */
	@Test
	void testFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAndFlush(S)}.
	 */
	@Test
	void testSaveAndFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#saveAllAndFlush(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllAndFlush() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllInBatchIterableOfT() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllByIdInBatch(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllByIdInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#deleteAllInBatch()}.
	 */
	@Test
	void testDeleteAllInBatch() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#getOne(java.lang.Object)}.
	 */
	@Test
	void testGetOne() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#getById(java.lang.Object)}.
	 */
	@Test
	void testGetById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindAllExampleOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.jpa.repository.JpaRepository#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllExampleOfSSort() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllSort1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.PagingAndSortingRepository#findAll(org.springframework.data.domain.Pageable)}.
	 */
	@Test
	void testFindAllPageable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#save(S)}.
	 */
	@Test
	void testSave() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#saveAll(java.lang.Iterable)}.
	 */
	@Test
	void testSaveAllIterableOfS1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findById(java.lang.Object)}.
	 */
	@Test
	void testFindById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#existsById(java.lang.Object)}.
	 */
	@Test
	void testExistsById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findAll()}.
	 */
	@Test
	void testFindAll1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#findAllById(java.lang.Iterable)}.
	 */
	@Test
	void testFindAllByIdIterableOfID1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#count()}.
	 */
	@Test
	void testCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteById(java.lang.Object)}.
	 */
	@Test
	void testDeleteById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#delete(java.lang.Object)}.
	 */
	@Test
	void testDelete() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAllById(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAll(java.lang.Iterable)}.
	 */
	@Test
	void testDeleteAllIterableOfQextendsT() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.CrudRepository#deleteAll()}.
	 */
	@Test
	void testDeleteAll() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findOne(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindOne() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testFindAllExampleOfS1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Sort)}.
	 */
	@Test
	void testFindAllExampleOfSSort1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#findAll(org.springframework.data.domain.Example, org.springframework.data.domain.Pageable)}.
	 */
	@Test
	void testFindAllExampleOfSPageable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#count(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testCountExampleOfS() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for
	 * {@link org.springframework.data.repository.query.QueryByExampleExecutor#exists(org.springframework.data.domain.Example)}.
	 */
	@Test
	void testExists() {
		fail("Not yet implemented");
	}

}
