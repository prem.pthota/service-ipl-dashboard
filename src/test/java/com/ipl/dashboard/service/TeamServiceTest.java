/**
 * 
 */
package com.ipl.dashboard.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.ipl.dashboard.domain.Team;
import com.ipl.dashboard.repository.TeamRepository;

/**
 * @author Polaiah Thota
 *
 */
@SpringBootTest
@DisplayName("When Running TeamService Class")
class TeamServiceTest {

	@MockBean
	private TeamRepository teamRepository;

	@Autowired
	private TeamService teamService;

	Team team;

	@BeforeEach
	void init() {
		team = new Team();
	}

	/**
	 * Test method for
	 * {@link com.ipl.dashboard.service.TeamService#getTeam(java.lang.String)}.
	 */
	@Test
	@DisplayName("Testing GetTeam Method")
	void testGetTeam() {
		
		String teamName = "CSK";
		team.setTeamName(teamName);

		when(teamRepository.findByTeamName("Chennai Super Kings")).thenReturn(team);
		assertEquals(teamName, teamService.getTeam("Chennai Super Kings").getTeamName());
	}

}
